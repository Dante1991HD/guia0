#include <iostream>
#include <list>
#include "Aminoacido.h"
using namespace std;

#ifndef CADENA_H
#define CADENA_H
// Definición de la clase cadena
class Cadena {
    //Declaracón de los atributos privados, la letra y sus aminoacidos asociados
    private:
        string letra  = "\0";
        list<Aminoacido> aminoacidos;
    //Declaracón de los metodos publicos, setters y getters, además del constructor
    public:
        Cadena(string letra);
        void set_letra(string letra);
        void add_aminoacido(Aminoacido aminoacido);
        string get_letra();
        list<Aminoacido> get_aminoacidos();
};
#endif
