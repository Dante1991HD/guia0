# Guia Numero 0 unidad I

## Descripcion del programa 
El programa genera una lista de proteinas, a estas se les puede agregar información sobre sus cadenas, aminoacidos y atomos. Permite tanto ingreso como mostrar en pantalla la totalidad de los datos ingresados.

## Requerimientos para utilizar el programa
* Sistema operativo Linux
* Compilador make

## Compilacion y ejecución del programa
* Abrir una terminal en el directorio donde se ubican los archivos del programa e ingresar el siguiente comando 
```
make
```
* Luego de completada la compilación utilice el siguiente comando
```
./programa
```

## Autor
* Dante Aguirre
* Correo: daguirre12@alumnos.utalca.cl
