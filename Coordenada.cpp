#include <iostream>
#include "Coordenada.h"
using namespace std;
//constructores

Coordenada::Coordenada() {
    float coor_x = 0;
    float coor_y = 0;
    float coor_z = 0;
}

Coordenada::Coordenada(float x, float y, float z) {
    this -> coor_x = x;
    this -> coor_y = y;
    this -> coor_z = z;
}

//metodos setters y getters
void Coordenada::set_x(float x) {
    this -> coor_x = x;
}

void Coordenada::set_y(float y) {
    this -> coor_y = y;
}

void Coordenada::set_z(float z) {
    this -> coor_z = z;
}

float Coordenada::get_x() {
    return this -> coor_x;
}

float Coordenada::get_y() {
    return this -> coor_y;
}

float Coordenada::get_z() {
    return this -> coor_z;
}