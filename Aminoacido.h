#include <iostream>
#include <list>
#include "Atomo.h"
using namespace std;

#ifndef AMINOACIDO_H
#define AMINOACIDO_H
// Definición de la clase aminoacido
class Aminoacido {
    //Declaracón de los atributos privados, nombre, número y atomos del aminoacido
    private:
        string nombre = "\0";
        int numero = 0;
        list <Atomo> atomos;
    //Declaracón de los metodos publicos, setters y getters, además del constructor
    public:
        Aminoacido(string nombre, int numero);
        void set_nombre(string nombre);
        void set_numero(int numero);
        void add_atomo(Atomo atomo);
        string get_nombre();
        int get_numero();
        list<Atomo> get_atomos();
};
#endif
