#include <iostream>
#include <list>
#include "Proteina.h"
#include "Cadena.h"
#include "Aminoacido.h"
#include "Atomo.h"
#include "Coordenada.h"
using namespace std;
//se declara la lista que contendrá a las proteinas
list < Proteina > proteinas;

//Función que toma datos para la confección del objeto proteina
void leer_datos_proteinas() {
    system("clear");
    //Se declaran las variables que contendrán los datos para los constructores
    string nombre, id, coordenada_x, coordenada_y, coordenada_z, letra;
    string cadenas, aminoaciods, atomos;
    //Se le pide al usuario el nombre y el ID para la proteina
    cout << "Ingrese el nombre de la proteina" << endl;
    getline(cin, nombre);
    cout << "Ingrese el ID de la proteina" << endl;
    getline(cin, id);
    //Se construlle proteina
    Proteina proteina = Proteina(nombre, id);
    cout << "¿Cuantas cadenas posee la proteina?" << endl;
    //Se pide al usuario la cantidad de cadenas a agregar a la proteina
    getline(cin, cadenas);
    //Se itera entre la cantidad de cadenas para agregar aminoacidos
    for (int i = 0; i < stoi(cadenas); i++) {
        //se pide la letra de la cadena
        cout << "Ingrese la letra de la cadena" << endl;
        getline(cin, letra);
        Cadena cadena = Cadena(letra);
        cout << "Ingrese la cantidad de aminoacidos de la cadena" << endl;
        getline(cin, aminoaciods);
        //se pide la cantidad de aminoacidos a agrega a la cadena y se itera 
        for (int j = 0; j < stoi(aminoaciods); j++) {
            /*Se piden los datos del constructor del aminoacido al igual que 
            la cantidad de atomos de este, para luego pedirlos en ciclo*/
            cout << "Ingrese el nombre del aminoacido numero " << j + 1 << endl;
            getline(cin, nombre);
            Aminoacido aminoacido = Aminoacido(nombre, j + 1);
            cout << "Ingrese la cantidad de atomos del aminoacido" << nombre << endl;
            getline(cin, atomos);
            //Finalmente en el ciclo de atomos se ingresan las coordenadas de los mismos
            for (int k = 0; k < stoi(atomos); k++) {
                cout << "Ingrese el nombre del atomo número " << k + 1 << endl;
                getline(cin, nombre);
                Atomo atomo = Atomo(nombre, k + 1);
                cout << "Ingrese la coordenada x del atomo" << endl;
                getline(cin, coordenada_x);
                cout << "Ingrese la coordenada y del atomo" << endl;
                getline(cin, coordenada_y);
                cout << "Ingrese la coordenada z del atomo" << endl;
                getline(cin, coordenada_z);
                Coordenada coor = Coordenada(stof(coordenada_x), stof(coordenada_y), stof(coordenada_z));
                /*ya construidos los objetos con sus datos correspondientes 
                se procede a crear la proteina asociando los objetos creados a sus listas correspondientes*/
                atomo.set_coordenada(coor);
                aminoacido.add_atomo(atomo);
            }
            cadena.add_aminoacido(aminoacido);
        }
        proteina.add_cadena(cadena);
    }
    proteinas.push_back(proteina);
}
//Función que imprime la totalidad de los datos guardados en la lista proteinas
void imprimir_datos_proteina() {
    system("clear");
    cout << "La/as proteina/as ingresada/as es/son:" << endl;
    //recorre la lista proteinas e impime en pantalla sus valores
    for (Proteina i: proteinas) {
        cout << "Nombre proteina: " << i.get_nombre() << endl;
        cout << "ID: " << i.get_id() << endl;
        //luego recupera los datos de la cadena e imprime sus valores
        for (Cadena c: i.get_cadena()) {
            cout << "Cadena " << c.get_letra() << ":" << endl;
            //de cadena se recuperan de manera iterativa los datos de aminoacido
            for (Aminoacido a: c.get_aminoacidos()) {
                cout << "Aminoacido: " << a.get_numero() << "-" << a.get_nombre() << ":" << endl;
                //de igual manera se recorre la lista atomo y se muestran sus atributos
                for (Atomo at: a.get_atomos()) {
                    cout << "" << at.get_nombre() << at.get_numero() << endl;
                    cout << "Coordenadas: ";
                    cout << "(" << at.get_coordenada().get_x() << ", ";
                    cout << at.get_coordenada().get_y() << ", ";
                    cout << at.get_coordenada().get_z() << ")" << endl;
                }
            }
        }
        cout << endl;
    }
    cout << endl;
}

int main(int argc, char * argv[]) {
    //booleano para el ciclo principal del programa
    bool on = true;
    //Variable de selección para el menú
    string opcion;
    cout << "Bienvenido al sistema rudimentario de base de datos proteicos" << endl;
    //Ciclo principal del programa
    while (on) {
        /* se da a escoger entre 3 opciones, las primeras dos son lo que pedía la guia, 
        la tercera cierra el programa*/
        cout << "¿Que es lo que desea hacer?" << endl;
        cout << "1-. Agregar proteína" << endl;
        cout << "2-. Imprimir proteínas" << endl;
        cout << "3-. Salir del programa" << endl;
        getline(cin, opcion);
        //Estrucctura switch llama a las 2 funciones principales del programa
        switch (stoi(opcion)) {
        case 1:
            leer_datos_proteinas();
            break;
        case 2:
            imprimir_datos_proteina();
            break;
        case 3:
            cout << "Hasta pronto" << endl;
            on = false;
            break;
        default:
            cout << "Ingrese una selección valida del menú por favor" << endl;
            break;
        }
    }
    return 0;
}