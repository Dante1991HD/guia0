#include <iostream>
#include <list>
#include "Cadena.h"
using namespace std;

#ifndef PROTEINA_H
#define PROTEINA_H
// Definición de la clase proteina
class Proteina {
    //Declaracón de los atributos privados, nombre, id y cadenas de la proteina
    private:
        string nombre = "\0";
        string id = "\0";
        list <Cadena> cadenas;
    //Declaracón de los metodos publicos, setters y getters, además del constructor
    public:
        Proteina(string nombre, string id);
        void set_nombre(string nombre);
        void set_id(string id);
        void add_cadena(Cadena cadena);
        string get_id();
        string get_nombre();
        list <Cadena> get_cadena();
};
#endif
