#include <iostream>
#include "Coordenada.h"
using namespace std;

#ifndef ATOMO_H
#define ATOMO_H
// Definición de la clase atomo
class Atomo {
    private:
        //Declaracón de los atributos privados, nombre, coordenadas y cantidad
        string nombre = "\0";
        int numero = 0;
        Coordenada coordenada;
     //Declaracón de los metodos publicos, setters y getters, además del constructor
    public:
        Atomo(string nombre, int numero);
        void set_nombre(string nombre);
        void set_numero(int numero);
        Coordenada get_coordenada();
        string get_nombre();
        int get_numero();
        void set_coordenada(Coordenada coordenada);
};
#endif
