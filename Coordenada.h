#include <iostream>
using namespace std;

#ifndef COORDENADA_H
#define COORDENADA_H
// Definición de la clase cadena
class Coordenada {
    //Declaracón de los atributos privados, en particular sus 3 coordenadas
    private:
        float coor_x = 0;
        float coor_y = 0;
        float coor_z = 0;
    //Declaracón de los metodos publicos, setters y getters, además de los constructores
    public:
        Coordenada();
        Coordenada(float x, float y, float z);
        void set_x(float x);
        void set_y(float y);
        void set_z(float z);
        float get_x();
        float get_y();
        float get_z();
};
#endif
